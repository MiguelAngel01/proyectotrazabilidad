package app;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pojos.Colorante;
import pojos.Material;
import pojos.Proveedores;

public class ColoranteController implements Initializable {

	ObservableList<Colorante> colorantes = FXCollections.observableArrayList();
	ObservableList<Proveedores> proveedores = FXCollections.observableArrayList();

	@FXML
	private TableView<Colorante> tvColorante;
	private TableColumn<Colorante, Integer> tcNumero;
	private TableColumn<Colorante, String> tcColor;
	private TableColumn<Colorante, String> tcLote;
	private TableColumn<Colorante, Proveedores> tcProveedor;
	private TableColumn<Colorante, LocalDate> tcFecha;
	@FXML
	private TextField tfNumero;
	@FXML
	private TextField tfColor;
	@FXML
	private TextField tfLote;
	@FXML
	private ChoiceBox<Proveedores> cbProveedor;
	@FXML
	private DatePicker dpFecha;
	@FXML
	private ChoiceBox<Proveedores> cbProveedorFilt;

	Session s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		s = Conexion.getSession();

		List<Colorante> coloranteList = s.createQuery("from Colorante", Colorante.class).list();
		colorantes.addAll(coloranteList);

		tcNumero = new TableColumn<>("Numero");
		tcColor = new TableColumn<>("Color");
		tcLote = new TableColumn<>("Lote");
		tcProveedor = new TableColumn<>("Proveedor");
		tcFecha = new TableColumn<>("Fecha Recepci�n");

		tcNumero.setCellValueFactory(new PropertyValueFactory<>("numero"));
		tcColor.setCellValueFactory(new PropertyValueFactory<>("color"));
		tcLote.setCellValueFactory(new PropertyValueFactory<>("lote"));
		tcProveedor.setCellValueFactory(new PropertyValueFactory<>("proveedores"));
		tcFecha.setCellValueFactory(new PropertyValueFactory<>("fechaRecepcion"));

		tvColorante.setItems(colorantes);

		tvColorante.getColumns().add(tcNumero);
		tvColorante.getColumns().add(tcColor);
		tvColorante.getColumns().add(tcLote);
		tvColorante.getColumns().add(tcProveedor);
		tvColorante.getColumns().add(tcFecha);

		List<Proveedores> proveedoresList = s
				.createQuery("from Proveedores WHERE Tipo = 'Colorante'", Proveedores.class).list();
		proveedores.addAll(proveedoresList);
		cbProveedor.setItems(proveedores);
		cbProveedorFilt.setItems(proveedores);
		
		tvColorante.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				if (tvColorante.getSelectionModel().getSelectedItem() != null) {
					Colorante colorante = tvColorante.getSelectionModel().getSelectedItem();
					tfNumero.setText(String.valueOf(colorante.getNumero()));
					tfColor.setText(colorante.getColor());
					tfLote.setText(colorante.getLote());
					cbProveedor.setValue(colorante.getProveedores());
					dpFecha.setValue(colorante.getFechaRecepcion());
				}

			}
		});

	}

	@FXML
	private void material() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("Material.fxml"));

			BorderPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.setTitle("Material");
			stage.show();

			stage = (Stage) this.tvColorante.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void insertar() {

		String numero = tfNumero.getText();
		String color = tfColor.getText();
		String lote = tfLote.getText();
		Proveedores proveedores = cbProveedor.getValue();
		LocalDate date = dpFecha.getValue();
		Transaction transaction = s.beginTransaction();
		try {
			Colorante colorante = new Colorante(Integer.parseInt(numero), color, lote, proveedores, date);
			s.save(colorante);
			colorantes.add(colorante);
			transaction.commit();

		} catch (NonUniqueObjectException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error en el campo N�mero");
			alert.setHeaderText("N�mero de material duplicado");
			alert.setContentText("Ya existe un material con el n�mero " + numero
					+ " asignado\n asigna otro n�mero al nuevo material, o eliminar el anterior");

			alert.showAndWait();
			transaction.rollback();
		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText("El campo n�mero solo puede contener numeros y no puede estar vac�o");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		}

	}

	@FXML
	private void eliminar() {

		Transaction transaction = s.beginTransaction();
		try {
			String numero = tfNumero.getText();
			Colorante colorante = s.get(Colorante.class, Integer.parseInt(numero));
			s.delete(colorante);
			colorantes.remove(colorante);
			transaction.commit();
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText(
					"El n�mero indicado no existe en la base de datos\ncomprueba el n�mero y prueba de nuevo");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText("El campo n�mero solo puede contener numeros y no puede estar vac�o");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		}

	}

	@FXML
	private void actualizar() {

		Transaction transaction = s.beginTransaction();
		try {
			String numero = tfNumero.getText();
			String color = tfColor.getText();
			String lote = tfLote.getText();
			Proveedores proveedores = cbProveedor.getValue();
			LocalDate date = dpFecha.getValue();
			Colorante colorante = s.get(Colorante.class, Integer.parseInt(numero));
			colorante.setColor(color);
			colorante.setLote(lote);
			colorante.setProveedores(proveedores);
			colorante.setFechaRecepcion(date);
			s.update(colorante);
			colorantes.set(tvColorante.getSelectionModel().getSelectedIndex(), colorante);
			transaction.commit();
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText(
					"El n�mero indicado no existe en la base de datos\ncomprueba el n�mero y prueba de nuevo");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText("El campo n�mero solo puede contener numeros y no puede estar vac�o");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		}

	}

	static Colorante colorante;

	@FXML
	private void buscar() {

		try {
			String numero = tfNumero.getText();
			colorante = s.get(Colorante.class, Integer.parseInt(numero));
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText(
					"El n�mero indicado no existe en la base de datos\ncomprueba el n�mero y prueba de nuevo");
			alert.setContentText("");

			alert.showAndWait();
		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText("El campo n�mero solo puede contener numeros y no puede estar vac�o");
			alert.setContentText("");

			alert.showAndWait();
		}

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("UsosColorante.fxml"));

			AnchorPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.setTitle("Usos");
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error al buscar usos");
			alert.setHeaderText("No has seleccionado ningun colorante");
			alert.setContentText(
					"Debes seleccionar un colorante en la tabla antes de buscar sus usos \npara seleccionar un colorante da clic sobre el en la tabla");

			alert.showAndWait();
		}

	}

	@FXML
	private void menu() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("MainMenu.fxml"));

			VBox escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.setTitle("MainMenu");
			stage.show();

			stage = (Stage) this.tvColorante.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void filtrar() {
		
		Proveedores proveedor = cbProveedorFilt.getValue();
		
		List<Colorante> coloranteList = s.createQuery("from Colorante WHERE proveedores = " + proveedor.getId(), Colorante.class).list();
		colorantes.removeAll(colorantes);
		colorantes.addAll(coloranteList);
		
	}
	
	@FXML
	private void eliminarFiltro() {
		
		List<Colorante> coloranteList = s.createQuery("from Colorante", Colorante.class).list();
		colorantes.removeAll(colorantes);
		colorantes.addAll(coloranteList);
		
	}

}
