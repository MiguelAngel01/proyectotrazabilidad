package app;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Session;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pojos.Colorante;
import pojos.Datosproducto;
import pojos.Material;
import pojos.Productos;

public class UsosController implements Initializable {

	ObservableList<Productos> lotes = FXCollections.observableArrayList();
	
	@FXML
	private TableView<Productos> tvUsos;
	private TableColumn<Productos, String> tcLote;
	private TableColumn<Productos, Datosproducto> tcDescripcion;
	private TableColumn<Productos, Colorante> tcColorante;
	private TableColumn<Productos, Material> tcMaterial;
	private TableColumn<Productos, LocalDateTime> tcFechaInicio;
	private TableColumn<Productos, LocalDateTime> tcFechaFin;
	private TableColumn<Productos, Integer> tcCantidad;
	private TableColumn<Productos, String> tcMaquina;
	
	Session s;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		s = Conexion.getSession();
		
		List<Productos> listaProducto = s.createQuery("FROM Productos WHERE material = " + MaterialController.material.getNumero(), Productos.class).list();
		lotes.addAll(listaProducto);

		tcLote = new TableColumn<>("Lote");
		tcDescripcion = new TableColumn<>("Descripcion");
		tcColorante = new TableColumn<>("Colorante");
		tcMaterial = new TableColumn<>("Material");
		tcFechaInicio = new TableColumn<>("Fecha Inicio");
		tcFechaFin = new TableColumn<>("Fecha Fin");
		tcCantidad = new TableColumn<>("Cantidad");
		tcMaquina = new TableColumn<>("Maquina");

		tcLote.setCellValueFactory(new PropertyValueFactory<>("lote"));
		tcDescripcion.setCellValueFactory(new PropertyValueFactory<>("datosproducto"));
		tcColorante.setCellValueFactory(new PropertyValueFactory<>("colorante"));
		tcMaterial.setCellValueFactory(new PropertyValueFactory<>("material"));
		tcFechaInicio.setCellValueFactory(new PropertyValueFactory<>("fechaInicio"));
		tcFechaFin.setCellValueFactory(new PropertyValueFactory<>("fechaFin"));
		tcCantidad.setCellValueFactory(new PropertyValueFactory<>("cantidad"));
		tcMaquina.setCellValueFactory(new PropertyValueFactory<>("maquina"));

		tvUsos.setItems(lotes);

		tvUsos.getColumns().add(tcLote);
		tvUsos.getColumns().add(tcDescripcion);
		tvUsos.getColumns().add(tcColorante);
		tvUsos.getColumns().add(tcMaterial);
		tvUsos.getColumns().add(tcFechaInicio);
		tvUsos.getColumns().add(tcFechaFin);
		tvUsos.getColumns().add(tcCantidad);
		tvUsos.getColumns().add(tcMaquina);
		
	}

}
