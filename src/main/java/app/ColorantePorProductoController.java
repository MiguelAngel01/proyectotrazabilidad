package app;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Session;

import pojos.Colorante;
import pojos.Datosproducto;
import pojos.Material;
import pojos.Productos;
import pojos.Proveedores;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ColorantePorProductoController implements Initializable {

	ObservableList<Colorante> colorantes = FXCollections.observableArrayList();
	ObservableList<Productos> lotes = FXCollections.observableArrayList();
	
	@FXML
	private TableView<Colorante> tvColorante;
	private TableColumn<Colorante, Integer> tcNumero;
	private TableColumn<Colorante, String> tcColor;
	private TableColumn<Colorante, String> tcLote;
	private TableColumn<Colorante, Proveedores> tcProveedor;
	private TableColumn<Colorante, LocalDate> tcFecha;
	
	@FXML
	private TableView<Productos> tvProductos;
	private TableColumn<Productos, String> tcLote1;
	private TableColumn<Productos, Datosproducto> tcDescripcion1;
	private TableColumn<Productos, Colorante> tcColorante;
	private TableColumn<Productos, Material> tcMaterial;
	private TableColumn<Productos, LocalDateTime> tcFechaInicio;
	private TableColumn<Productos, LocalDateTime> tcFechaFin;
	private TableColumn<Productos, Integer> tcCantidad;
	private TableColumn<Productos, String> tcMaquina;
	
	Session s;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		s = Conexion.getSession();
		
		colorantes.add(Maquina200DMController.colorante);

		tcNumero = new TableColumn<>("Numero");
		tcColor = new TableColumn<>("Color");
		tcLote = new TableColumn<>("Lote");
		tcProveedor = new TableColumn<>("Proveedor");
		tcFecha = new TableColumn<>("Fecha Recepción");

		tcNumero.setCellValueFactory(new PropertyValueFactory<>("numero"));
		tcColor.setCellValueFactory(new PropertyValueFactory<>("color"));
		tcLote.setCellValueFactory(new PropertyValueFactory<>("lote"));
		tcProveedor.setCellValueFactory(new PropertyValueFactory<>("proveedores"));
		tcFecha.setCellValueFactory(new PropertyValueFactory<>("fechaRecepcion"));

		tvColorante.setItems(colorantes);

		tvColorante.getColumns().add(tcNumero);
		tvColorante.getColumns().add(tcColor);
		tvColorante.getColumns().add(tcLote);
		tvColorante.getColumns().add(tcProveedor);
		tvColorante.getColumns().add(tcFecha);
		
		List<Productos> listaProducto = s.createQuery("FROM Productos WHERE colorante = " + Maquina200DMController.colorante.getNumero(), Productos.class).list();
		lotes.addAll(listaProducto);

		tcLote1 = new TableColumn<>("Lote");
		tcDescripcion1 = new TableColumn<>("Descripcion");
		tcColorante = new TableColumn<>("Colorante");
		tcMaterial = new TableColumn<>("Material");
		tcFechaInicio = new TableColumn<>("Fecha Inicio");
		tcFechaFin = new TableColumn<>("Fecha Fin");
		tcCantidad = new TableColumn<>("Cantidad");
		tcMaquina = new TableColumn<>("Maquina");

		tcLote1.setCellValueFactory(new PropertyValueFactory<>("lote"));
		tcDescripcion1.setCellValueFactory(new PropertyValueFactory<>("datosproducto"));
		tcColorante.setCellValueFactory(new PropertyValueFactory<>("colorante"));
		tcMaterial.setCellValueFactory(new PropertyValueFactory<>("material"));
		tcFechaInicio.setCellValueFactory(new PropertyValueFactory<>("fechaInicio"));
		tcFechaFin.setCellValueFactory(new PropertyValueFactory<>("fechaFin"));
		tcCantidad.setCellValueFactory(new PropertyValueFactory<>("cantidad"));
		tcMaquina.setCellValueFactory(new PropertyValueFactory<>("maquina"));

		tvProductos.setItems(lotes);

		tvProductos.getColumns().add(tcLote1);
		tvProductos.getColumns().add(tcDescripcion1);
		tvProductos.getColumns().add(tcColorante);
		tvProductos.getColumns().add(tcMaterial);
		tvProductos.getColumns().add(tcFechaInicio);
		tvProductos.getColumns().add(tcFechaFin);
		tvProductos.getColumns().add(tcCantidad);
		tvProductos.getColumns().add(tcMaquina);
		
	}

}
