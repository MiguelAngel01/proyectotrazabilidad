package app;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class Conexion {
	private static Session sesion;

	public static Session getSession() {
		if (sesion == null) {
			StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
			try {
				SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
				sesion = sessionFactory.openSession();
			} catch (Exception e) {
				StandardServiceRegistryBuilder.destroy(registry);
			}
		}
		return sesion;
	}

	public static void closeSession() {
		if (sesion != null) {
			sesion.close();
			sesion = null;
		}
	}
}
