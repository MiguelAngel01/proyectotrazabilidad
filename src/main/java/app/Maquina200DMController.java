package app;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.ResourceBundle;

import javax.persistence.PersistenceException;

import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import pojos.Colorante;
import pojos.Datosproducto;
import pojos.Material;
import pojos.Productos;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Maquina200DMController implements Initializable {

	ObservableList<Productos> lotes = FXCollections.observableArrayList();
	ObservableList<Material> materiales = FXCollections.observableArrayList();
	ObservableList<Colorante> colorantes = FXCollections.observableArrayList();
	ObservableList<Datosproducto> descripciones = FXCollections.observableArrayList();

	@FXML
	private TableView<Productos> tv200DM;
	private TableColumn<Productos, String> tcLote;
	private TableColumn<Productos, Datosproducto> tcDescripcion;
	private TableColumn<Productos, Colorante> tcColorante;
	private TableColumn<Productos, Material> tcMaterial;
	private TableColumn<Productos, LocalDateTime> tcFechaInicio;
	private TableColumn<Productos, LocalDateTime> tcFechaFin;
	private TableColumn<Productos, Integer> tcCantidad;
	@FXML
	private ChoiceBox<Material> cbMaterial;
	@FXML
	private ChoiceBox<Colorante> cbColor;
	@FXML
	private TextField tfLote;
	@FXML
	private TextField tfCantidad;
	@FXML
	private Spinner<Integer> spHoraInicio;
	@FXML
	private Spinner<Integer> spMinutoInicio;
	@FXML
	private Spinner<Integer> spHoraFin;
	@FXML
	private Spinner<Integer> spMinutoFin;
	@FXML
	private DatePicker dpInicio;
	@FXML
	private DatePicker dpFin;
	@FXML
	private ChoiceBox<String> cbMaquina;
	@FXML
	private ChoiceBox<Datosproducto> cbDescripcion;

	Session s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		s = Conexion.getSession();

		
		
		List<Material> listaMateriales = s.createQuery("FROM Material", Material.class).list();
		materiales.addAll(listaMateriales);
		cbMaterial.setItems(materiales);
		List<Colorante> listaColorantes = s.createQuery("FROM Colorante", Colorante.class).list();
		colorantes.addAll(listaColorantes);
		cbColor.setItems(colorantes);
		List<Datosproducto> listaDescripciones = s.createQuery("FROM Datosproducto", Datosproducto.class).list();
		descripciones.addAll(listaDescripciones);
		cbDescripcion.setItems(descripciones);

		tcLote = new TableColumn<>("Lote");
		tcDescripcion = new TableColumn<>("Descripcion");
		tcColorante = new TableColumn<>("Colorante");
		tcMaterial = new TableColumn<>("Material");
		tcFechaInicio = new TableColumn<>("Fecha Inicio");
		tcFechaFin = new TableColumn<>("Fecha Fin");
		tcCantidad = new TableColumn<>("Cantidad");

		tcLote.setCellValueFactory(new PropertyValueFactory<>("lote"));
		tcDescripcion.setCellValueFactory(new PropertyValueFactory<>("datosproducto"));
		tcColorante.setCellValueFactory(new PropertyValueFactory<>("colorante"));
		tcMaterial.setCellValueFactory(new PropertyValueFactory<>("material"));
		tcFechaInicio.setCellValueFactory(new PropertyValueFactory<>("fechaInicio"));
		tcFechaFin.setCellValueFactory(new PropertyValueFactory<>("fechaFin"));
		tcCantidad.setCellValueFactory(new PropertyValueFactory<>("cantidad"));

		tcFechaFin.setMinWidth(125);
		tcFechaFin.setPrefWidth(125);
		tcFechaInicio.setMinWidth(125);
		tcFechaInicio.setPrefWidth(125);
		tcDescripcion.setMinWidth(100);
		tcDescripcion.setPrefWidth(100);
		tv200DM.setItems(lotes);

		tv200DM.getColumns().add(tcLote);
		tv200DM.getColumns().add(tcDescripcion);
		tv200DM.getColumns().add(tcColorante);
		tv200DM.getColumns().add(tcMaterial);
		tv200DM.getColumns().add(tcFechaInicio);
		tv200DM.getColumns().add(tcFechaFin);
		tv200DM.getColumns().add(tcCantidad);

		SpinnerValueFactory<Integer> valueFactory1 = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 23, 0);
		SpinnerValueFactory<Integer> valueFactory2 = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 23, 0);
		SpinnerValueFactory<Integer> valueFactory3 = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 59, 0);
		SpinnerValueFactory<Integer> valueFactory4 = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 59, 0);

		spHoraInicio.setValueFactory(valueFactory1);
		spHoraFin.setValueFactory(valueFactory2);
		spMinutoInicio.setValueFactory(valueFactory3);
		spMinutoFin.setValueFactory(valueFactory4);

		tv200DM.getSelectionModel().selectedItemProperty()
				.addListener((ChangeListener) (observable, oldValue, newValue) -> {

					if (tv200DM.getSelectionModel().getSelectedItem() != null) {
						Productos producto = tv200DM.getSelectionModel().getSelectedItem();
						cbMaterial.setValue(producto.getMaterial());
						cbColor.setValue(producto.getColorante());
						tfLote.setText(producto.getLote());
						tfCantidad.setText(String.valueOf(producto.getCantidad()));
						dpInicio.setValue(LocalDate.from(producto.getFechaInicio()));
						dpFin.setValue(LocalDate.from(producto.getFechaFin()));
						SpinnerValueFactory<Integer> value1 = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 23,
								producto.getFechaInicio().getHour());
						SpinnerValueFactory<Integer> value2 = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 23,
								producto.getFechaFin().getHour());
						SpinnerValueFactory<Integer> value3 = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 59,
								producto.getFechaInicio().getMinute());
						SpinnerValueFactory<Integer> value4 = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 59,
								producto.getFechaFin().getMinute());
						spHoraInicio.setValueFactory(value1);
						spHoraFin.setValueFactory(value2);
						spMinutoInicio.setValueFactory(value3);
						spMinutoFin.setValueFactory(value4);
						cbDescripcion.setValue(producto.getDatosproducto());
					}

				});

		ObservableList<String> maquinas = FXCollections.observableArrayList("200DM", "200AITIAN", "250", "150", "80");
		cbMaquina.setItems(maquinas);

		cbMaquina.getSelectionModel().selectedItemProperty()
				.addListener((ChangeListener) (observable, oldValue, newValue) -> {

					if (cbMaquina.getSelectionModel().getSelectedItem() != null) {
						String maquina = cbMaquina.getValue();
						List<Productos> listaProducto = s
								.createQuery("FROM Productos WHERE maquina = '" + maquina + "'", Productos.class)
								.list();
						lotes.removeAll(lotes);
						lotes.addAll(listaProducto);
					}

				});

	}

	@FXML
	private void insertar() {

		String lote = tfLote.getText();
		String tipo = cbDescripcion.getValue().getTipo();
		Transaction transaction = s.beginTransaction();
		try {
			switch (tipo) {
			case "Simple":
				if (lote.charAt(0) == 'L') {
					Datosproducto descripcion = cbDescripcion.getValue();
					int cantidad = Integer.parseInt(tfCantidad.getText());
					LocalTime horaInicio = LocalTime.of(spHoraInicio.getValue(), spMinutoInicio.getValue());
					LocalDateTime fechaInicio = LocalDateTime.of(dpInicio.getValue(), horaInicio);
					LocalTime horaFin = LocalTime.of(spHoraFin.getValue(), spMinutoFin.getValue());
					LocalDateTime fechaFin = LocalDateTime.of(dpFin.getValue(), horaFin);
					Material material = cbMaterial.getValue();
					Colorante colorante = cbColor.getValue();
					String maquina = cbMaquina.getValue();

					Productos producto = new Productos(lote, colorante, descripcion, material, fechaInicio, fechaFin,
							cantidad, maquina);
					s.save(producto);
					lotes.add(producto);
					transaction.commit();
				} else {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Error de sintaxis en el campo Lote");
					alert.setHeaderText("El producto es de tipo Simple por lo que el campo lote debe comenzar por L");
					alert.setContentText("");

					alert.showAndWait();
					transaction.rollback();
				}
				break;
			case "Ensamblado":
				if (lote.charAt(0) == 'M') {
					Datosproducto descripcion = cbDescripcion.getValue();
					int cantidad = Integer.parseInt(tfCantidad.getText());
					LocalTime horaInicio = LocalTime.of(spHoraInicio.getValue(), spMinutoInicio.getValue());
					LocalDateTime fechaInicio = LocalDateTime.of(dpInicio.getValue(), horaInicio);
					LocalTime horaFin = LocalTime.of(spHoraFin.getValue(), spMinutoFin.getValue());
					LocalDateTime fechaFin = LocalDateTime.of(dpFin.getValue(), horaFin);
					Material material = cbMaterial.getValue();
					Colorante colorante = cbColor.getValue();
					String maquina = cbMaquina.getValue();

					Productos producto = new Productos(lote, colorante, descripcion, material, fechaInicio, fechaFin,
							cantidad, maquina);
					s.save(producto);
					transaction.commit();
					lotes.add(producto);
				} else {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Error de sintaxis en el campo Lote");
					alert.setHeaderText(
							"El producto es de tipo Ensamblado por lo que el campo lote debe comenzar por M");
					alert.setContentText("");

					alert.showAndWait();
					transaction.rollback();
				}
				break;
			default:
				break;
			}
		} catch (NonUniqueObjectException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error en el campo Lote");
			alert.setHeaderText("Lote duplicado");
			alert.setContentText("Ya existe un lote con el identificador " + lote
					+ " asignado\n asigna otro lote o elimina el anterior");

			alert.showAndWait();
			transaction.rollback();
		}

	}

	@FXML
	private void actaulizar() {

		Productos producto = s.get(Productos.class, tv200DM.getSelectionModel().getSelectedItem().getLote());

		String lote = tfLote.getText();
		Transaction transaction = s.beginTransaction();
		try {
			if (producto.getLote().equalsIgnoreCase(lote)) {
				Datosproducto descripcion = cbDescripcion.getValue();
				int cantidad = Integer.parseInt(tfCantidad.getText());
				LocalTime horaInicio = LocalTime.of(spHoraInicio.getValue(), spMinutoInicio.getValue());
				LocalDateTime fechaInicio = LocalDateTime.of(dpInicio.getValue(), horaInicio);
				LocalTime horaFin = LocalTime.of(spHoraFin.getValue(), spMinutoFin.getValue());
				LocalDateTime fechaFin = LocalDateTime.of(dpFin.getValue(), horaFin);
				Material material = cbMaterial.getValue();
				Colorante colorante = cbColor.getValue();
				String maquina = cbMaquina.getValue();

				producto.setCantidad(cantidad);
				producto.setDatosproducto(descripcion);
				producto.setColorante(colorante);
				producto.setMaterial(material);
				producto.setFechaFin(fechaFin);
				producto.setFechaInicio(fechaInicio);

				s.update(producto);
				lotes.set(tv200DM.getSelectionModel().getSelectedIndex(), producto);
				transaction.commit();
			}
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo Lote");
			alert.setHeaderText(
					"El lote indicado no existe en la base de datos\ncomprueba el lote y prueba de nuevo");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		}

	}

	@FXML
	private void eliminar() {

		Transaction transaction = s.beginTransaction();
		try {
			Productos producto = s.get(Productos.class, tv200DM.getSelectionModel().getSelectedItem().getLote());
			s.delete(producto);
			for (int i = 0; i < lotes.size(); i++) {
				if (lotes.get(i).getLote() == producto.getLote()) {
					lotes.remove(i);
					break;
				}
			}			
			transaction.commit();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo Lote");
			alert.setHeaderText(
					"El lote indicado no existe en la base de datos\ncomprueba el lote y prueba de nuevo");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		}

	}

	static Material material;

	@FXML
	private void material() {

		Productos producto = s.get(Productos.class, tv200DM.getSelectionModel().getSelectedItem().getLote());
		material = producto.getMaterial();

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("MaterialPorProducto.fxml"));

			AnchorPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.setTitle("Material");
			stage.show();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error al buscar material");
			alert.setHeaderText("No has seleccionado ningun lote");
			alert.setContentText("Debes seleccionar un lote en la tabla antes de buscar su material\npara seleccionar un lote da clic sobre el en la tabla");

			alert.showAndWait();
		}

	}

	static Colorante colorante;

	@FXML
	private void colorante() {

		Productos producto = s.get(Productos.class, tv200DM.getSelectionModel().getSelectedItem().getLote());
		colorante = producto.getColorante();

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("ColorantePorProducto.fxml"));

			AnchorPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.setTitle("Material");
			stage.show();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error al buscar colorante");
			alert.setHeaderText("No has seleccionado ningun lote");
			alert.setContentText("Debes seleccionar un lote en la tabla antes de buscar su colorante\npara seleccionar un lote da clic sobre el en la tabla");

			alert.showAndWait();
		}

	}
	
	@FXML
	private void menu() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("MainMenu.fxml"));

			VBox escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.setTitle("MainMenu");
			stage.show();

			stage = (Stage) this.tv200DM.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
