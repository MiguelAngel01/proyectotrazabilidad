package app;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pojos.Colorante;
import pojos.Material;
import pojos.Productos;
import pojos.Proveedores;

public class MaterialController implements Initializable {

	ObservableList<Material> materiales = FXCollections.observableArrayList();
	ObservableList<String> descripciones = FXCollections.observableArrayList();
	ObservableList<Proveedores> proveedores = FXCollections.observableArrayList();

	static List<Productos> productos;

	@FXML
	private TableView<Material> tvMaterial;
	private TableColumn<Material, Integer> tcNumero;
	private TableColumn<Material, String> tcDescripcion;
	private TableColumn<Material, String> tcLote;
	private TableColumn<Material, Proveedores> tcProveedor;
	private TableColumn<Material, LocalDate> tcFecha;
	@FXML
	private TextField tfNumero;
	@FXML
	private ChoiceBox<String> cbDescripcion;
	@FXML
	private TextField tfLote;
	@FXML
	private ChoiceBox<Proveedores> cbProveedor;
	@FXML
	private DatePicker dpFecha;
	@FXML
	private ChoiceBox<Proveedores> cbProveedorFilt;

	Session s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		s = Conexion.getSession();

		List<Material> materialList = s.createQuery("from Material", Material.class).list();
		materiales.addAll(materialList);

		tcNumero = new TableColumn<>("Numero");
		tcDescripcion = new TableColumn<>("Descripci�n");
		tcLote = new TableColumn<>("Lote");
		tcProveedor = new TableColumn<>("Proveedor");
		tcFecha = new TableColumn<>("Fecha Recepci�n");

		tcNumero.setCellValueFactory(new PropertyValueFactory<>("numero"));
		tcDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
		tcLote.setCellValueFactory(new PropertyValueFactory<>("lote"));
		tcProveedor.setCellValueFactory(new PropertyValueFactory<>("proveedores"));
		tcFecha.setCellValueFactory(new PropertyValueFactory<>("fechaRecepcion"));

		tvMaterial.setItems(materiales);

		tvMaterial.getColumns().add(tcNumero);
		tvMaterial.getColumns().add(tcDescripcion);
		tvMaterial.getColumns().add(tcLote);
		tvMaterial.getColumns().add(tcProveedor);
		tvMaterial.getColumns().add(tcFecha);

		List<Proveedores> proveedoresList = s.createQuery("from Proveedores WHERE Tipo = 'Material'", Proveedores.class)
				.list();
		proveedores.addAll(proveedoresList);
		cbProveedor.setItems(proveedores);
		cbProveedorFilt.setItems(proveedores);

		descripciones.add("HOMO");
		descripciones.add("RANDOM");
		cbDescripcion.setItems(descripciones);
		cbDescripcion.setValue("HOMO");

		tvMaterial.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				if (tvMaterial.getSelectionModel().getSelectedItem() != null) {
					Material material = tvMaterial.getSelectionModel().getSelectedItem();
					tfNumero.setText(String.valueOf(material.getNumero()));
					cbDescripcion.setValue(material.getDescripcion());
					tfLote.setText(material.getLote());
					cbProveedor.setValue(material.getProveedores());
					dpFecha.setValue(material.getFechaRecepcion());
				}

			}
		});

	}

	@FXML
	private void colorante() {

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("Colorante.fxml"));

			BorderPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.setTitle("Colorante");
			stage.show();

			stage = (Stage) this.tvMaterial.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@FXML
	private void insertar() {

		Transaction transaction = s.beginTransaction();
		String numero = tfNumero.getText();
		String descipcion = cbDescripcion.getValue();
		String lote = tfLote.getText();
		Proveedores proveedores = cbProveedor.getValue();
		LocalDate date = dpFecha.getValue();
		try {
			Material material = new Material(Integer.parseInt(numero), descipcion, lote, proveedores, date);
			s.save(material);
			materiales.add(material);
			transaction.commit();
		} catch (NonUniqueObjectException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error en el campo N�mero");
			alert.setHeaderText("N�mero de material duplicado");
			alert.setContentText("Ya existe un material con el n�mero " + numero
					+ " asignado\n asigna otro n�mero al nuevo material, o eliminar el anterior");

			alert.showAndWait();
			transaction.rollback();
		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText("El campo n�mero solo puede contener numeros y no puede estar vac�o");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		}

	}

	@FXML
	private void eliminar() {

		Transaction transaction = s.beginTransaction();
		try {
			String numero = tfNumero.getText();
			Material material = s.get(Material.class, Integer.parseInt(numero));
			s.delete(material);
			materiales.remove(material);
			transaction.commit();
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText(
					"El n�mero indicado no existe en la base de datos\ncomprueba el n�mero y prueba de nuevo");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText("El campo n�mero solo puede contener numeros y no puede estar vac�o");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		}

	}

	@FXML
	private void actualizar() {

		Transaction transaction = s.beginTransaction();
		try {
			String numero = tfNumero.getText();
			String descipcion = cbDescripcion.getValue();
			String lote = tfLote.getText();
			Proveedores proveedores = cbProveedor.getValue();
			LocalDate date = dpFecha.getValue();
			Material material = s.get(Material.class, Integer.parseInt(numero));
			material.setDescripcion(descipcion);
			material.setLote(lote);
			material.setProveedores(proveedores);
			material.setFechaRecepcion(date);
			s.update(material);
			materiales.set(tvMaterial.getSelectionModel().getSelectedIndex(), material);
			transaction.commit();
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText(
					"El n�mero indicado no existe en la base de datos\ncomprueba el n�mero y prueba de nuevo");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText("El campo n�mero solo puede contener numeros y no puede estar vac�o");
			alert.setContentText("");

			alert.showAndWait();
			transaction.rollback();
		}

	}

	static Material material;

	@FXML
	private void buscar() {

		try {
			String numero = tfNumero.getText();
			material = s.get(Material.class, Integer.parseInt(numero));
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText(
					"El n�mero indicado no existe en la base de datos\ncomprueba el n�mero y prueba de nuevo");
			alert.setContentText("");

			alert.showAndWait();
		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de sintaxis en el campo N�mero");
			alert.setHeaderText("El campo n�mero solo puede contener numeros y no puede estar vac�o");
			alert.setContentText("");

			alert.showAndWait();
		}

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("Usos.fxml"));

			AnchorPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.setTitle("Usos");
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error al buscar usos");
			alert.setHeaderText("No has seleccionado ningun material");
			alert.setContentText(
					"Debes seleccionar un material en la tabla antes de buscar sus usos \npara seleccionar un colorante da clic sobre el en la tabla");

			alert.showAndWait();
		}

	}
	
	@FXML
	private void menu() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("MainMenu.fxml"));

			VBox escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.setTitle("MainMenu");
			stage.show();

			stage = (Stage) this.tvMaterial.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void filtrar() {
		
		Proveedores proveedor = cbProveedorFilt.getValue();
		
		List<Material> materialList = s.createQuery("from Material WHERE proveedores = " + proveedor.getId(), Material.class).list();
		materiales.removeAll(materiales);
		materiales.addAll(materialList);
		
	}
	
	@FXML
	private void eliminarFiltro() {
		
		List<Material> materialList = s.createQuery("from Material", Material.class).list();
		materiales.removeAll(materiales);
		materiales.addAll(materialList);
		
	}
	
}
