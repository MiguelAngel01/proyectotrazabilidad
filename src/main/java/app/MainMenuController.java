package app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MainMenuController implements Initializable{

	@FXML
	private Button material;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

	@FXML
	private void materiasPrimas() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("MateriasPrimas.fxml"));

			VBox escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.setTitle("Material");
			stage.show();
			
			stage = (Stage) this.material.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void fabricacion() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("200DM.fxml"));

			BorderPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.setTitle("Maquina 200DM");
			stage.show();
			
			stage = (Stage) this.material.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
