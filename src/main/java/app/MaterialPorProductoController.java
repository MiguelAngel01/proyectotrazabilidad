package app;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Session;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pojos.Colorante;
import pojos.Datosproducto;
import pojos.Material;
import pojos.Productos;
import pojos.Proveedores;

public class MaterialPorProductoController implements Initializable {

	ObservableList<Material> materiales = FXCollections.observableArrayList();
	ObservableList<Productos> lotes = FXCollections.observableArrayList();

	@FXML
	private TableView<Material> tvMaterial;
	private TableColumn<Material, Integer> tcNumero;
	private TableColumn<Material, String> tcDescripcion;
	private TableColumn<Material, String> tcLote;
	private TableColumn<Material, Proveedores> tcProveedor;
	private TableColumn<Material, LocalDate> tcFecha;

	@FXML
	private TableView<Productos> tvProductos;
	private TableColumn<Productos, String> tcLote1;
	private TableColumn<Productos, Datosproducto> tcDescripcion1;
	private TableColumn<Productos, Colorante> tcColorante;
	private TableColumn<Productos, Material> tcMaterial;
	private TableColumn<Productos, LocalDateTime> tcFechaInicio;
	private TableColumn<Productos, LocalDateTime> tcFechaFin;
	private TableColumn<Productos, Integer> tcCantidad;
	private TableColumn<Productos, String> tcMaquina;

	Session s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		s = Conexion.getSession();

		materiales.addAll(Maquina200DMController.material);

		tcNumero = new TableColumn<>("Numero");
		tcDescripcion = new TableColumn<>("Descripción");
		tcLote = new TableColumn<>("Lote");
		tcProveedor = new TableColumn<>("Proveedor");
		tcFecha = new TableColumn<>("Fecha Recepción");

		tcNumero.setCellValueFactory(new PropertyValueFactory<>("numero"));
		tcDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
		tcLote.setCellValueFactory(new PropertyValueFactory<>("lote"));
		tcProveedor.setCellValueFactory(new PropertyValueFactory<>("proveedores"));
		tcFecha.setCellValueFactory(new PropertyValueFactory<>("fechaRecepcion"));

		tvMaterial.setItems(materiales);

		tvMaterial.getColumns().add(tcNumero);
		tvMaterial.getColumns().add(tcDescripcion);
		tvMaterial.getColumns().add(tcLote);
		tvMaterial.getColumns().add(tcProveedor);
		tvMaterial.getColumns().add(tcFecha);
		
		List<Productos> listaProducto = s.createQuery("FROM Productos WHERE material = " + Maquina200DMController.material.getNumero(), Productos.class).list();
		lotes.addAll(listaProducto);

		tcLote1 = new TableColumn<>("Lote");
		tcDescripcion1 = new TableColumn<>("Descripcion");
		tcColorante = new TableColumn<>("Colorante");
		tcMaterial = new TableColumn<>("Material");
		tcFechaInicio = new TableColumn<>("Fecha Inicio");
		tcFechaFin = new TableColumn<>("Fecha Fin");
		tcCantidad = new TableColumn<>("Cantidad");
		tcMaquina = new TableColumn<>("Maquina");

		tcLote1.setCellValueFactory(new PropertyValueFactory<>("lote"));
		tcDescripcion1.setCellValueFactory(new PropertyValueFactory<>("datosproducto"));
		tcColorante.setCellValueFactory(new PropertyValueFactory<>("colorante"));
		tcMaterial.setCellValueFactory(new PropertyValueFactory<>("material"));
		tcFechaInicio.setCellValueFactory(new PropertyValueFactory<>("fechaInicio"));
		tcFechaFin.setCellValueFactory(new PropertyValueFactory<>("fechaFin"));
		tcCantidad.setCellValueFactory(new PropertyValueFactory<>("cantidad"));
		tcMaquina.setCellValueFactory(new PropertyValueFactory<>("maquina"));

		tvProductos.setItems(lotes);

		tvProductos.getColumns().add(tcLote1);
		tvProductos.getColumns().add(tcDescripcion1);
		tvProductos.getColumns().add(tcColorante);
		tvProductos.getColumns().add(tcMaterial);
		tvProductos.getColumns().add(tcFechaInicio);
		tvProductos.getColumns().add(tcFechaFin);
		tvProductos.getColumns().add(tcCantidad);
		tvProductos.getColumns().add(tcMaquina);

	}

}
