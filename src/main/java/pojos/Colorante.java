package pojos;
// Generated 12 abr. 2021 17:18:20 by Hibernate Tools 5.4.27.Final

import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Colorante generated by hbm2java
 */
public class Colorante implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int numero;
	private Proveedores proveedores;
	private String color;
	private String lote;
	private LocalDate fechaRecepcion;
	private Set<Productos> productoses = new HashSet<Productos>(0);

	public Colorante() {
	}

	public Colorante(int numero, String color, String lote, Proveedores proveedores, LocalDate fechaRecepcion) {
		this.numero = numero;
		this.color = color;
		this.lote = lote;
		this.proveedores = proveedores;
		this.fechaRecepcion = fechaRecepcion;
	}
	
	public Colorante(int numero, String color) {
		this.numero = numero;
		this.color = color;
	}

	public Colorante(int numero, Proveedores proveedores, String color, String lote, Set<Productos> productoses) {
		this.numero = numero;
		this.proveedores = proveedores;
		this.color = color;
		this.lote = lote;
		this.productoses = productoses;
	}

	public int getNumero() {
		return this.numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Proveedores getProveedores() {
		return this.proveedores;
	}

	public void setProveedores(Proveedores proveedores) {
		this.proveedores = proveedores;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getLote() {
		return this.lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public Set<Productos> getProductoses() {
		return this.productoses;
	}

	public void setProductoses(Set<Productos> productoses) {
		this.productoses = productoses;
	}
	
	public LocalDate getFechaRecepcion() {
		return fechaRecepcion;
	}
	
	public void setFechaRecepcion(LocalDate fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	@Override
	public String toString() {
		return numero + " - " + color;
	}
	
}
